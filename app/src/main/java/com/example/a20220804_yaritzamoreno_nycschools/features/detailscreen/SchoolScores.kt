package com.example.a20220804_yaritzamoreno_nycschools.features.detailscreen

data class SchoolScores(
    val dbn: String,
    val math: Double,
    val reading: Double,
    val writing: Double
)