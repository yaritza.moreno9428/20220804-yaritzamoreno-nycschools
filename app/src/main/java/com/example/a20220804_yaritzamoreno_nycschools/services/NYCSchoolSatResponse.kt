package com.example.a20220804_yaritzamoreno_nycschools.services


import com.example.a20220804_yaritzamoreno_nycschools.features.detailscreen.SchoolScores
import com.google.gson.annotations.SerializedName

data class NYCSchoolSatResponse(
    @SerializedName("dbn")
    val dbn: String?,
    @SerializedName("num_of_sat_test_takers")
    val numOfSatTestTakers: String?,
    @SerializedName("sat_critical_reading_avg_score")
    val satCriticalReadingAvgScore: String?,
    @SerializedName("sat_math_avg_score")
    val satMathAvgScore: String?,
    @SerializedName("sat_writing_avg_score")
    val satWritingAvgScore: String?,
    @SerializedName("school_name")
    val schoolName: String?
){
    companion object {
        fun NYCSchoolSatResponse.toDomain(): SchoolScores{
            return SchoolScores(
                dbn = dbn?:"",
                math = satMathAvgScore?.toDoubleOrNull()?:0.0,
                reading = satCriticalReadingAvgScore?.toDoubleOrNull()?:0.0,
                writing = satWritingAvgScore?.toDoubleOrNull()?:0.0
            )
        }
    }
}