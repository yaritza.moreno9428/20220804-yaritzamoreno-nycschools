package com.example.a20220804_yaritzamoreno_nycschools

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class NycApplication: Application() {
}