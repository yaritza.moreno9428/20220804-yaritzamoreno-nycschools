package com.example.a20220804_yaritzamoreno_nycschools.injection

import com.example.a20220804_yaritzamoreno_nycschools.domain.NYCRepository
import com.example.a20220804_yaritzamoreno_nycschools.services.NYC_BASE_URL
import com.example.a20220804_yaritzamoreno_nycschools.services.NetworkRepository
import com.example.a20220804_yaritzamoreno_nycschools.services.SchoolsApi
import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Provides
    @Singleton
    fun providesRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl(NYC_BASE_URL)
            .addConverterFactory(GsonConverterFactory.create(Gson()))
            .client(
                OkHttpClient.Builder()
                    .build()
            ).build()
    }

    @Provides
    @Singleton
    fun providesNycApi(retrofit: Retrofit): SchoolsApi {
        return retrofit.create(SchoolsApi::class.java)
    }

    @Provides
    @Singleton
    fun providesNYCRepository(api: SchoolsApi): NYCRepository {
        return NetworkRepository(api)
    }
}