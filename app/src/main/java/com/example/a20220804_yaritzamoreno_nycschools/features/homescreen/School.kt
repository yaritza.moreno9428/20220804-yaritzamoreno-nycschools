package com.example.a20220804_yaritzamoreno_nycschools.features.homescreen

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class School(
    val dbn: String,
    val name: String,
    val overview: String,

): Parcelable
