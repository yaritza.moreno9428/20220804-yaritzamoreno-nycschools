package com.example.a20220804_yaritzamoreno_nycschools.services

import com.example.a20220804_yaritzamoreno_nycschools.domain.NYCRepository
import com.example.a20220804_yaritzamoreno_nycschools.features.detailscreen.SchoolScores
import com.example.a20220804_yaritzamoreno_nycschools.features.homescreen.School
import com.example.a20220804_yaritzamoreno_nycschools.services.NYCSchoolResponse.Companion.toDomain
import com.example.a20220804_yaritzamoreno_nycschools.services.NYCSchoolSatResponse.Companion.toDomain
import javax.inject.Inject

class NetworkRepository @Inject constructor(
    private val api: SchoolsApi
): NYCRepository {
    override suspend fun getAllNYCSchools(): List<School>? {
        val response = api.getNYCSchools()
        return when(response.isSuccessful){
            true -> response.body()?.map { it.toDomain() }
            false -> null
        }
    }

    override suspend fun getNYCSchoolScore(dbn: String): SchoolScores? {
        val response = api.getNYCSchoolSat(dbn)
        return when(response.isSuccessful){
            true -> response.body()?.firstOrNull()?.toDomain()
            false -> null
        }
    }
}