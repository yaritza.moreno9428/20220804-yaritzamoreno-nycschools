package com.example.a20220804_yaritzamoreno_nycschools.features.detailscreen

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import com.example.a20220804_yaritzamoreno_nycschools.R
import com.example.a20220804_yaritzamoreno_nycschools.databinding.FragmentDetailBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class DetailFragment : Fragment() {
    private lateinit var _binding: FragmentDetailBinding
    private val navArgs by navArgs<DetailFragmentArgs>()
    private val detailViewModel by viewModels<DetailViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_detail, container, false)
        _binding = FragmentDetailBinding.bind(view)
        with(_binding){
            schoolName.text = navArgs.school.name
            fullOverview.text = navArgs.school.overview
        }
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        detailViewModel.schoolScores.observe(viewLifecycleOwner){
            with(_binding){
                readingScore.text = getString(R.string.score_reading, it.reading)
                mathScore.text = getString(R.string.score_math, it.math)
                writingScore.text = getString(R.string.score_writing, it.writing)

            }
        }
        detailViewModel.getSchoolScores(navArgs.school)
    }

}