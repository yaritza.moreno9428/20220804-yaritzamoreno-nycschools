package com.example.a20220804_yaritzamoreno_nycschools.services

import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

const val NYC_BASE_URL = "https://data.cityofnewyork.us/"

interface SchoolsApi {

    @GET("resource/s3k6-pzi2.json")
    suspend fun getNYCSchools(): Response<List<NYCSchoolResponse>>

    @GET("resource/f9bf-2cp4.json")
    suspend fun getNYCSchoolSat(
        @Query("dbn") dbn: String
    ): Response<List<NYCSchoolSatResponse>>

}