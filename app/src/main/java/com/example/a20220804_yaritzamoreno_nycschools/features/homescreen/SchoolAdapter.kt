package com.example.a20220804_yaritzamoreno_nycschools.features.homescreen

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.a20220804_yaritzamoreno_nycschools.databinding.SchoolItemviewBinding

typealias SchoolListener = (School) -> Unit
class SchoolAdapter(
    private val onSchoolClick: SchoolListener
): ListAdapter<School, SchoolAdapter.SchoolVH>(SchoolCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SchoolVH {
        return SchoolVH(
            SchoolItemviewBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: SchoolVH, position: Int) {
        holder.bind(getItem(position))
    }

    inner class SchoolVH(private val binding: SchoolItemviewBinding): RecyclerView.ViewHolder(binding.root) {
        fun bind(school: School){
            with(binding){
                name.text = school.name
                description.text = school.overview
                showDetails.setOnClickListener { onSchoolClick(school) }
            }
        }
    }

    class SchoolCallback(): DiffUtil.ItemCallback<School>(){
        override fun areItemsTheSame(oldItem: School, newItem: School): Boolean = oldItem.dbn == newItem.dbn

        override fun areContentsTheSame(oldItem: School, newItem: School): Boolean = oldItem == newItem
    }
}