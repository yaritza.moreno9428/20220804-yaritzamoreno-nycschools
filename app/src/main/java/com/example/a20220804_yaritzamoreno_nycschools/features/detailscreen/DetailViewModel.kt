package com.example.a20220804_yaritzamoreno_nycschools.features.detailscreen

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.a20220804_yaritzamoreno_nycschools.domain.NYCRepository
import com.example.a20220804_yaritzamoreno_nycschools.features.homescreen.School
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class DetailViewModel @Inject constructor(
    private val nycRepository: NYCRepository
) : ViewModel() {
    private val _schoolScores = MutableLiveData<SchoolScores>()
    val schoolScores: LiveData<SchoolScores>
        get() = _schoolScores

    fun getSchoolScores(school: School) {
        viewModelScope.launch(Dispatchers.IO) {
            nycRepository.getNYCSchoolScore(school.dbn)?.let {
                _schoolScores.postValue(it)
            }
        }
    }
}