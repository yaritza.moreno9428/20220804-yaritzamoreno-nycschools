package com.example.a20220804_yaritzamoreno_nycschools.features.homescreen

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.example.a20220804_yaritzamoreno_nycschools.R
import com.example.a20220804_yaritzamoreno_nycschools.databinding.FragmentHomeBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class HomeFragment : Fragment() {
    private lateinit var _binding: FragmentHomeBinding
    private val adapter by lazy { SchoolAdapter(::onSchoolClicked) }
    private val navController by lazy { findNavController() }
    private val homeViewModel by viewModels<HomeViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_home, container, false)
        _binding = FragmentHomeBinding.bind(view)
        with(_binding){
            rvSchools.adapter = adapter
        }
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        homeViewModel.schoolList.observe(viewLifecycleOwner){ newList ->
            adapter.submitList(newList)
        }
        homeViewModel.loadAllSchools()

    }

    private fun onSchoolClicked(school: School){
        navController.navigate(HomeFragmentDirections.actionHomeScreenToDetailScreen(school))
    }

}