package com.example.a20220804_yaritzamoreno_nycschools.domain

import com.example.a20220804_yaritzamoreno_nycschools.features.detailscreen.SchoolScores
import com.example.a20220804_yaritzamoreno_nycschools.features.homescreen.School

interface NYCRepository {
    suspend fun getAllNYCSchools(): List<School>?
    suspend fun getNYCSchoolScore(dbn: String): SchoolScores?
}